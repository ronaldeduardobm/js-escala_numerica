
function crearEscala() {
  var numero = document.getElementById("entrada").value;
  var mult = crearOperacion(0, numero);
  var divi = crearOperacion(1, mult[0]);
  var tabla = "<table class='center'>";
  tabla += "<tr>" +
    "<th colspan=\"3\"> Escala del numero "+ numero+ "</th>" +
    "</tr>"+
    "<tr>" +
    "<th colspan=\"3\"> Muliplicación</th>" +
    "</tr>"+
    "<tr>" +
    "<th> Dato 1 </th>" +
    "<th> Dato 2</th>" +
    "<th> Resultado</th>" +
    "</tr>";
  tabla += mult[1];
  tabla+= "<tr>"+
  "<th colspan=\"3\"> Division </th>" +
  "</tr>"+
  "<tr>"+
  "<th> Dato 1 </th>" +
  "<th> Dato 2</th>" +
  "<th> Resultado</th>" +
  "</tr>";
  tabla += divi[1];
  tabla += "</table>";
  document.getElementById("rta").innerHTML = tabla;
}
function crearOperacion(operacion, numero) {
  let vectorRta = new Array(2);
  let rta = "";
  for (var i = 1; i <= 10; i++) {
    var operacionResultado = 0;
    if (operacion == 0)
      operacionResultado = numero * i;
    else
      operacionResultado = numero / i;
    numero = operacionResultado;
    rta += "\n<tr>";
    rta += "\n<td>" + numero + "</td>";
    rta += "\n<td>" + i + "</td>";
    rta += "\n<td>" + operacionResultado + "</td>";
    rta += "\n</tr>";
  }
  vectorRta[0] = numero;
  vectorRta[1] = rta;
  return vectorRta;
}
